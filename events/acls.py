import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):

    headers = {'Authorization': PEXELS_API_KEY}
    params = {
        "query": f'{city}, {state}',
        "per_page": "1"
    }

    url = 'https://api.pexels.com/v1/search'

    response = requests.get(url, headers=headers, params=params)
    # Option 1 for parsing json (using json library to parse)
    # clue = json.loads(response.content)+
    # Option 2 for parsing json (Using the requests package to parse)
    r = response.json()
    # Create a dictionary of data to use containing
    photo = {

        "picture_url": r["photos"][0]["src"]["original"]
    }
    # return the dictionary
    try:
        return photo
    except(KeyError, IndexError):
        return {"picture_url":None}


def get_weather_data(city):

    url ='http://api.openweathermap.org/geo/1.0/direct'


    params = {

        "q": {city},
        "appid": OPEN_WEATHER_API_KEY
    }

    response = requests.get(url, params=params)
    content = response.json

    long_lat = {
        "lat": content["lat"],
        "lon": content["lon"]
    }

    return long_lat
